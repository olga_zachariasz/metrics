/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Mateusz Baran
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
import bmec.components.*;
import bmec.metrics.BPMNMetric;

import java.lang.Integer;
import java.lang.System;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;

public class PSMMetric extends BPMNMetric {

	@Override
	public String getName() {
		return "PSM Metric";
	}

	@Override
	public double calculate(Diagram d) {
		ArrayList<Integer> occurrences = new ArrayList<>();
		// System.out.println(d.getName());
		for (String key : d.getElements().keySet()) {
			occurrences.add(d.getElements().get(key).getCounter());
//			System.out.println(key + " "
//					+ d.getElements().get(key).getCounter());
		}

		Collections.sort(occurrences);
		Collections.reverse(occurrences);
		// System.out.println(d.getName());
//		System.out.println("occurrences to array 1:"
//				+ Arrays.toString(occurrences.toArray()));

		for (int i = 0; i < occurrences.size() - 1; ++i) {
			occurrences.set(i + 1, occurrences.get(i) + occurrences.get(i + 1));
		}

//		System.out.println("occurrences to array 2: "
//				+ Arrays.toString(occurrences.toArray()));

		for (int i = occurrences.size() - 1; i >= 0; --i) {
//			System.out.println(i + " : " + occurrences.get(i) + " ? "
//					+ ((i + 1) * (i + 1)));
			if (occurrences.get(i) >= (i + 1) * (i + 1)) {
//				System.out.println(i + " : " + occurrences.get(i) + ">="
//						+ ((i + 1) * (i + 1)));
				return i + 1;
			}
		}
		return 0;
	}
}