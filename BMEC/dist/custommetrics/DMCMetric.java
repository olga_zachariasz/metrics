import java.util.Set;

import bmec.components.Diagram;
import bmec.metrics.BPMNMetric;

/**
 * 
 * @author olga
 * 
 */
public class DMCMetric extends BPMNMetric {

	@Override
	public String getName() {
		return "Different Modeling Concepts Metric";
	}

	/**
	 * Count types of elements in diagram.
	 */
	@Override
	public double calculate(Diagram d) {
		Set<String> elementsKeys = d.getElements().keySet();

		return elementsKeys.size();
	}

}
