
/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Mateusz Baran
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

import bmec.components.Diagram;
import bmec.components.ECollection;
import bmec.metrics.BPMNMetric;
import bmec.metrics.manager.*;

import java.util.ArrayList;
import java.util.Set;

public class NOACMetric extends BPMNMetric {

	@Override
	public String getName() {
		return "Number of Activities and Control Flow Elements in a Process Metric";
	}

	public static final ArrayList<String> activities = new ArrayList<String>();
	static {
		activities.add("Service Task");
		activities.add("Activity");
		activities.add("Subprocess");
		activities.add("Business Rule Task");
		activities.add("Send Event Task");
		activities.add("User Task");
		activities.add("Call activity");
		activities.add("Script Task");
		activities.add("Parallel Gateway");
		activities.add("Data-based XOR Gateway");
		activities.add("Default Start Event");
		activities.add("Default End Event");
		activities.add("Default Boundary Event");
		activities.add("Start Timer Event");
		activities.add("Start Signal Event");
		activities.add("Start Error Event");
		activities.add("Start Escalation Event");
		activities.add("Start Cancelation Event");
		activities.add("Start Message Event");
		activities.add("Start Conditional Event");
		activities.add("Default Boundary Event");
		activities.add("Boundary Timer Event");
		activities.add("Boundary Signal Event");
		activities.add("Boundary Error Event");
		activities.add("Boundary Escalation Event");
		activities.add("Boundary Cancelation Event");
		activities.add("Boundary Message Event");
		activities.add("End Timer Event");
		activities.add("End Signal Event");
		activities.add("End Error Event");
		activities.add("End Escalation Event");
		activities.add("End Some Event");
		activities.add("End Message Event");
		activities.add("End Conditional Event");
		activities.add("Default Intermediate Catch Event");
		activities.add("Default Intermediate Throw Event");
		activities.add("Intermediate Throw");
		activities.add("Intermediate Throw Timer Event");
		activities.add("Intermediate Throw Signal Event");
		activities.add("Intermediate Throw Error Event");
		activities.add("Intermediate Throw Escalation Event");
		activities.add("Intermediate Throw Some Event");
		activities.add("Intermediate Throw Message Event");
		activities.add("Intermediate Throw Conditional Event");
		activities.add("Intermediate Catch");
		activities.add("Intermediate Catch Timer Event");
		activities.add("Intermediate Catch Signal Event");
		activities.add("Intermediate Catch Error Event");
		activities.add("Intermediate Catch Escalation Event");
		activities.add("Intermediate Catch Some Event");
		activities.add("Intermediate Catch Message Event");
		activities.add("Intermediate Catch Conditional Event");
		activities.add("Event-based XOR Gateway");
		activities.add("Data-based OR Gateway");
	}

	@Override
	public double calculate(Diagram d) {
		int sum = 0;

		for (String key : d.getElements().keySet()) {
			ECollection elem = d.getElements().get(key);
			if (activities.contains(elem.getName())) {
				sum += elem.getCounter();
			}
		}

		return sum;
	}
}