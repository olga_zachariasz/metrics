import bmec.components.Diagram;
import bmec.components.Element;
import bmec.components.FlowElement;
import bmec.components.GatewayElement;
import bmec.metrics.BPMNMetric;

/**
 * 
 * @author olga
 * 
 */
public class AGDMetric extends BPMNMetric {

	@Override
	public String getName() {
		return "Average Gateway Degree";
	}

	@Override
	public double calculate(Diagram d) {
		double agdValue = 0;
		int totalIncAndOuts = 0;
		int gatewayCounter = 0;
		for (Element elem : d.getUniqueElements()) {
			if (elem instanceof GatewayElement) {
				GatewayElement ge = (GatewayElement) elem;
				totalIncAndOuts += ge.getInc() + ge.getOut();
				gatewayCounter++;
			}
		}
		if (gatewayCounter == 0) {
			agdValue = 0;
		} else {
			agdValue = totalIncAndOuts / gatewayCounter;
		}

		return agdValue;

	}
}
