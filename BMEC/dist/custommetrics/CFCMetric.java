import bmec.components.Diagram;
import bmec.components.Element;
import bmec.components.GatewayElement;
import bmec.metrics.BPMNMetric;

/**
 * 
 * @author olga
 * 
 */
public class CFCMetric extends BPMNMetric {

	@Override
	public String getName() {
		return "Control Flow Complexity Metric";
	}

	@Override
	public double calculate(Diagram d) {
		int cfcValue = 0;
		for (Element elem : d.getUniqueElements()) {
			if (elem instanceof GatewayElement) {
				GatewayElement ge = (GatewayElement) elem;
				if (ge.getGatewayType().equals("Diverging")) {
					int numOfOuts = ge.getOut();
					switch (ge.getType()) {
					case "parallelGateway": // AND
						cfcValue += 1;
						break;
					case "inclusiveGateway": // OR
						cfcValue += Math.pow(2.0, numOfOuts) - 1;
						break;
					case "exclusiveGateway": // XOR
					case "eventBasedGateway":
						cfcValue += numOfOuts;
						break;
					}

				}
			}
		}

		return cfcValue;
	}
}
