import java.util.ArrayList;

import bmec.components.Diagram;
import bmec.components.ECollection;
import bmec.metrics.BPMNMetric;

/**
 * 
 * @author olga
 *
 */
public class NOR extends BPMNMetric {

	@Override
	public String getName() {
		return "Number Of Roles Metric";
	}

	private static final ArrayList<String> activities = new ArrayList<String>();
	static {
		activities.add("Lane");
	}

	@Override
	public double calculate(Diagram d) {
		int sum = 0;
		for (String key : d.getElements().keySet()) {
			ECollection elem = d.getElements().get(key);
			if (activities.contains(elem.getName()))
				sum += elem.getCounter();
		}
		return sum;
	}

}
