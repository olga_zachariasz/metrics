import bmec.components.Diagram;
import bmec.components.Element;
import bmec.components.GatewayElement;
import bmec.metrics.BPMNMetric;

/**
 * 
 * @author olga
 * 
 */
public class CCMetric extends BPMNMetric {

	static final int FLOW = 1;
	static final int XOR_2 = 2;
	static final int XOR_3 = 3;
	static final int AND = 4;
	static final int OR = 7;
	static final int SUBTASK = 2;
	static final int MULTIPLE_INSTANCE_ACT = 6;
	static final int CANCELATION = 1;

	@Override
	public String getName() {
		return "Cognitive Complexity Metric";
	}

	@Override
	public double calculate(Diagram d) {
		int ccValue = 0;

		for (Element elem : d.getUniqueElements()) {
			String type = elem.getType();
			if (elem instanceof GatewayElement) {
				switch (type) {
				case "parallelGateway": // AND
					ccValue += AND;
					break;
				case "inclusiveGateway": // OR
					ccValue += OR;
					break;
				case "exclusiveGateway": // XOR
				case "eventBasedGateway":
					GatewayElement gatewayEl = (GatewayElement) elem;
					int inc = gatewayEl.getInc();
					int out = gatewayEl.getOut();
					String gatewayType = gatewayEl.getGatewayType();

					if (gatewayType.equals("Diverging")) {
						if (out > 2)
							ccValue += XOR_3;
						else
							ccValue += XOR_2;
					} else if (gatewayType.equals("Converging")) {
						if (inc > 2)
							ccValue += XOR_3;
						else
							ccValue += XOR_2;
					}
					break;
				}
			} else {
				switch (type) {
				case "subProcess":
					ccValue += SUBTASK;
					break;
				case "sequenceFlow":
					ccValue += FLOW;
					break;
				case "multiInstanceLoopCharacteristics":
					ccValue += MULTIPLE_INSTANCE_ACT;
					break;
				case "startEventcancelEventDefinition":
				case "boundaryEventcancelEventDefinition":
				case "intermediateThrowEventcancelEventDefinition":
				case "intermediateCatchEventcancelEventDefinition":
				case "endEventcancelEventDefinition":
					ccValue += CANCELATION;
					break;
				default:
					break;
				}
			}

		}

		return ccValue;
	}

}
