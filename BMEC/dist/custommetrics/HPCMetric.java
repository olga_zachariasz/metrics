import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import bmec.components.Diagram;
import bmec.components.ECollection;
import bmec.components.Element;
import bmec.metrics.BPMNMetric;

/**
 * 
 * @author olga
 * 
 */
public class HPCMetric extends BPMNMetric {

	private static final List<String> dataElements = new ArrayList<String>();
	static {
		dataElements.add("Data Object");
		dataElements.add("Data Association");
		dataElements.add("Data Store");
		dataElements.add("Data Input");
		dataElements.add("Data Output");
	}

	@Override
	public String getName() {
		return "Halstead-based Process Complexity";

	}

	@Override
	public double calculate(Diagram d) {

		double N1 = new NOACMetric().calculate(d); // N1 is number of total
													// activities and control
													// flows

		double n1 = 0; // n2 is number of unique activities and control flows

		double N2 = 0; // N2 is number of total data elements

		double n2 = 0; // n2 is number of unique data elements

		Map<String, ECollection> elementsMapInProcess = d.getElements();
		List<Element> uniqueElementsInProcess = d.getUniqueElements();

		Set<Element> uniqueDataElements = new HashSet<Element>();
		Set<Element> uniqueControlElements = new HashSet<Element>();

		for (Element elem : uniqueElementsInProcess) {
			ECollection element = elementsMapInProcess.get(elem.getType());
			if (dataElements.contains(element.getName())) {
				N2++;
				uniqueDataElements.add(elem);
			} else if ((NOACMetric.activities).contains(element.getName())) {
				uniqueControlElements.add(elem);
			}
		}

		n1 = uniqueControlElements.size();
		n2 = uniqueDataElements.size();

		double hpcD = 0;
		double hpcN = 0;
		double hpcV = 0;

		if (n2 == 0) {
			hpcD = (n1 / 2);
		} else {
			hpcD = (n1 / 2) * (N2 / n2);
		}

		// in case of log(0) 0 is return
		// (workaround for situation when NaN was returned as hpc value)
		hpcN = (n1 * log(n1)) + (n2 * log(n2));
		hpcV = (N1 + N2) * log(n1 + n2);

		System.out.println(d.getName());
		System.out
				.println("N1 " + N1 + " n1 " + n1 + " N2 " + N2 + " n2 " + n2);
		System.out.println("N=" + hpcN + " D=" + hpcD + " V=" + hpcV);
		return hpcD;
	}

	// log with base 2
	private double log(double num) {
		if (num == 0)
			return 0;
		else
			return Math.log(num) / Math.log(2);
	}
}
