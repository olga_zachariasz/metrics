

/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Rados�aw O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
import bmec.components.*;
import bmec.metrics.BPMNMetric;
/**
 * Simple example class presenting access to elements summarized in the diagram.
 * @author izeos (radek dot milo at gmail dot com)
 */
public class SimpleMetric extends BPMNMetric {

    @Override
public String getName(){return "All Elements";}

    @Override
    public double calculate(Diagram d) {
		double score=0;

		for(String key: d.getElements().keySet()){
			score+=d.getElements().get(key).getCounter();
		}

		return score;
    }
}