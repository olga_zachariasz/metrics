

/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Rados�aw O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
import bmec.components.*;
import bmec.metrics.BPMNMetric;
import bmec.metrics.manager.*;

import java.lang.Math;
import java.lang.String;
import java.lang.System;
import java.util.ArrayList;

/**
 * Simple example class presenting access to elements summarized in the diagram.
 * @author izeos (radek dot milo at gmail dot com)
 */
public class ICMetric extends BPMNMetric {

    @Override
    public String getName(){return "Interface Complexity Metric";}

    private static final ArrayList<String> inputs = new ArrayList<String>(),
        outputs = new ArrayList<String>();
    static {
        inputs.add("Default Start Event");
        inputs.add("Start Timer Event");
        inputs.add("Start Signal Event");
        inputs.add("Start Error Event");
        inputs.add("Start Escalation Event");
        inputs.add("Start Cancelation Event");
        inputs.add("Start Message Event");
        inputs.add("Start Conditional Event");

        outputs.add("Default End Event");
        outputs.add("End Timer Event");
        outputs.add("End Signal Event");
        outputs.add("End Error Event");
        outputs.add("End Escalation Event");
        outputs.add("End Some Event");
        outputs.add("End Message Event");
        outputs.add("End Conditional Event");
    }

    @Override
    public double calculate(Diagram d) {
        //by length we mean hear the number of elements in a diagram, by similarity to number of lines of code
        //used in original metric
        //boundary events are not taken into account
        double length = .0, nInputs = .0, nOutputs = .0;

        for(String key: d.getElements().keySet()){
            ECollection elem = d.getElements().get(key);
            if(inputs.contains(elem.getName()))
                nInputs += elem.getCounter();
            if(outputs.contains(elem.getName()))
                nOutputs += elem.getCounter();
        }


        BPMNMetric loaded = MetricsManager.getMetrics().get("NOAMetric");
        length = loaded.calculate(d);

        //System.out.println(" " + d.getName());
        //System.out.println(" " + length + " " + nInputs + " " + nOutputs);

        double nn = nInputs * nOutputs;
        return length * nn * nn;
    }
}