import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import bmec.components.Diagram;
import bmec.components.Element;
import bmec.components.FlowElement;
import bmec.metrics.BPMNMetric;

public class DiameterMetric extends BPMNMetric {

	String delimeter = "/";

	@Override
	public String getName() {
		return "Diameter Metric";
	}

	@Override
	public double calculate(Diagram d) {
		System.out.println("Calculate for " + d.getName());

		Map<String, String> mapReferences = new HashMap<String, String>(); // <src;trg>
		Map<String, Integer> mapDecisionPoints = new LinkedHashMap<String, Integer>(); // <id_of_decision_point;
																						// choosen_decision_index>

		boolean startup = true;
		int diameter = 0; // the longest path
		int pathLength = 0; // temp var for each path length
		String decisionsPath = ""; // temp var for each path sequence

		String startId = ""; // id of start element
		String src = ""; // id of current source element

		for (Element elem : d.getUniqueElements()) {
			if (elem instanceof FlowElement) {
				FlowElement flowEl = ((FlowElement) elem);
				String srcRef = flowEl.getSourceRef();
				String trgRef = flowEl.getTargetRef();

				if (mapReferences.containsKey(srcRef)) {
					String currentTargetRef = mapReferences.get(srcRef);
					mapReferences.put(srcRef, currentTargetRef + delimeter
							+ trgRef);
				} else {
					mapReferences.put(srcRef, "" + trgRef);
				}
			} else {
				if (elem.getType().contains("startEvent")) {
					startId = elem.getId();
				}
			}
		}

		/**
		 * take care of boundary events which are attached to other elements.
		 * targets of boundary events will be copied as a targets of element to
		 * which boundary event is attached to.
		 */
		for (Map.Entry<String, String> entry : d.getAttachedElements()
				.entrySet()) {
			String key = entry.getKey(); // id of boundary event
			String value = entry.getValue(); // id of element to which event is
												// attached to
			String eventTargets = mapReferences.get(key); 
			String attachedToTarget = mapReferences.get(value); 
			mapReferences.put(value, attachedToTarget + delimeter
					+ eventTargets);
			mapReferences.remove(key);
		}

		while (startup
				|| (mapDecisionPoints.size() > 0 && isMapContainingNonZero(mapDecisionPoints))) {
			pathLength = 0;
			startup = false;
			src = startId;
			decisionsPath = delimeter + src + delimeter;
			while (mapReferences.get(src) != null) {
				String target = mapReferences.get(src);
				String[] possibleTargets = target.split(delimeter);
				int numberOfPossibleTargets = possibleTargets.length;
				int decisionIndex = 1;

				/**
				 * If more than one possible decisions can be made from this
				 * element
				 **/
				if (numberOfPossibleTargets > 1) {
					if (mapDecisionPoints.containsKey(src)) {
						/** get last decision **/
						decisionIndex = mapDecisionPoints.get(src);
						/**
						 * if true - lately last possible decision was made and
						 * there is no more decision to choose from this element
						 **/
						if (decisionIndex == numberOfPossibleTargets) {
							mapDecisionPoints.put(src, 0); // unselect this
															// element
							break;

						} else {
							/** else - there is next decision which can be made **/
							decisionIndex++;
							mapDecisionPoints.put(src, decisionIndex);
						}
					} else {
						/** add new element to decision points map **/
						mapDecisionPoints.put(src, decisionIndex);
					}
				}

				/** get id of next element **/
				src = possibleTargets[decisionIndex - 1];

				/**
				 * check if path does not contain cycle if cycle exists then
				 * diameter cannot be computed
				 */
				if (!src.isEmpty()
						&& decisionsPath.contains(delimeter + src + delimeter)) {
					System.out
							.println("DIAMETER CANNOT BE COMPUTED BECAUSE OF EXISTING CYCLES");
					return 0;
				}
				decisionsPath += src + delimeter;
				pathLength++;
			} // end of while inner

			if (diameter < pathLength) {
				diameter = pathLength;
			}

		} // end of while outer

		System.out.println("DIAMETER IS " + diameter);

		return diameter;
	}

	/**
	 * Function checks if map contains 0 in its values set. Used to check if all
	 * decision points in diagram have been chosen. If map has only 0 values
	 * then there is no more decision. If there is at least one value greater
	 * than 0 then there is some decisions to be made.
	 * 
	 * @param mapa
	 * @return
	 */
	public boolean isMapContainingNonZero(Map<String, Integer> mapa) {

		Collection<Integer> values = mapa.values();
		for (Integer integer : values) {
			if (integer > 0) {
				return true;
			}
		}
		return false;
	}

}
