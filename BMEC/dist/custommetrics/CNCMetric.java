

/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Mateusz Baran
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

import bmec.components.Diagram;
import bmec.components.ECollection;
import bmec.metrics.BPMNMetric;
import bmec.metrics.manager.*;

import java.util.ArrayList;

public class CNCMetric extends BPMNMetric {

    @Override
    public String getName(){return "Coefficient of Network Complexity";}

    private static final ArrayList<String> arcs = new ArrayList<String>();
    static {
        arcs.add("Sequence Flow");
        //arcs.add("Message Flow");
    }

    @Override
    public double calculate(Diagram d) {
        int nArcs = 0;
        for(String key: d.getElements().keySet()){
            ECollection elem = d.getElements().get(key);
            if(arcs.contains(elem.getName()))
                nArcs += elem.getCounter();
        }

        BPMNMetric loaded = MetricsManager.getMetrics().get("NOAJSMetric");

        return nArcs/loaded.calculate(d);
    }
}