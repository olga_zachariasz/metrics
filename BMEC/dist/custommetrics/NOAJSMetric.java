

/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Mateusz Baran
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */

import bmec.components.Diagram;
import bmec.components.ECollection;
import bmec.metrics.BPMNMetric;
import bmec.metrics.manager.*;

import java.util.ArrayList;

public class NOAJSMetric extends BPMNMetric {

    @Override
    public String getName(){return "Number of Activities, Joins and Splits in a Process Metric";}

    private static final ArrayList<String> activities = new ArrayList<String>();
    static {
        activities.add("Service Task");
        activities.add("Activity");
        activities.add("Subprocess");
        activities.add("Business Rule Task");
        activities.add("Send Event Task");
        activities.add("User Task");
        activities.add("Call activity");
        activities.add("Script Task");
        activities.add("Parallel Gateway");
        activities.add("Data-based XOR Gateway");
        activities.add("Event-based XOR Gateway");
        activities.add("Data-based OR Gateway");
    }

    @Override
    public double calculate(Diagram d) {
        int sum = 0;
        for(String key: d.getElements().keySet()){
            ECollection elem = d.getElements().get(key);
            if(activities.contains(elem.getName()))
                sum += elem.getCounter();
        }
        return sum;
    }
}