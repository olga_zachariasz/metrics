/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Radosław O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package bmec.metrics.manager;

import bmec.components.Diagram;
import bmec.metrics.BPMNMetric;
import java.io.File;
import java.io.FilenameFilter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * 
 * @author izeos (radek dot milo at gmail dot com)
 */
public class MetricsManagerImpl extends MetricsManager {

	private Map<Object, Map<String, Object>> scores;
	private File directory;

	/**
	 * 
	 * @param path
	 *            The path to the directory with metrics.
	 * @param searched
	 *            Names of metrics classes to calculate.
	 */
	public MetricsManagerImpl(String path, final List<String> searched) {
		directory = new File(path);
		if (!(directory.exists() && directory.isDirectory())) {
			directory.mkdir();
			// return; could spare some time but requires additional
			// initializations or conditions
		}
		metrics = loadMetrics(directory);
		inputMetrics = verifyInputs(searched);
		scores = new HashMap<>();
	}

	private Map<String, BPMNMetric> loadMetrics(File d) {
		Map<String, BPMNMetric> m = new HashMap<>();
		for (File f : d.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				return dir.equals(directory) && name.endsWith(".class");
			}
		})) {

			try {
				URLClassLoader classLoader = URLClassLoader
						.newInstance(new URL[] { directory.toURI().toURL() });
				Class<?> myClass = Class.forName(
						f.getName().replaceFirst(".class", ""), true,
						classLoader);
				BPMNMetric metricInstance = (BPMNMetric) myClass.newInstance();
				m.put(myClass.getName(), metricInstance);
			} catch (MalformedURLException | ClassNotFoundException
					| InstantiationException | IllegalAccessException ex) {
				Logger.getLogger(MetricsManagerImpl.class.getName()).log(
						Level.SEVERE, null, ex);
			}
		}
		return m;
	}

	private List<String> verifyInputs(List<String> tmp) {
		for (String metricName : tmp) {
			if (!metrics.containsKey(metricName)) {
				tmp.remove(metricName);
			}
		}
		return tmp;
	}

	@Override
	public void calculate(Diagram d) {
		Map<String, Object> diagramScores = new HashMap<>();
		scores.put(d, diagramScores);

		for (String metricName : inputMetrics) {
			double result = metrics.get(metricName).calculate(d);
			diagramScores.put(metricName, result);
		}
	}

	@Override
	public Map<Object, Map<String, Object>> getScores() {
		return scores;
	}
}
