/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Radosław O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package bmec.metrics.manager;

import bmec.components.Diagram;
import bmec.metrics.BPMNMetric;
import java.util.List;
import java.util.Map;

/**
 *
 * @author izeos (radek dot milo at gmail dot com)
 */
public abstract class MetricsManager {

    /**
     *
     */
    protected static List<String> inputMetrics;
    /**
     *
     */
    protected static Map<String, BPMNMetric> metrics;

    /**
     *
     */
    public static List<String> getInputMetrics() {
	return inputMetrics;
    }

    /**
     *
     */
    public static void setInputMetrics(List<String> inputMetrics) {
	MetricsManager.inputMetrics = inputMetrics;
    }

    /**
     *
     * @return map containing enable metrics
     */
    public static Map<String, BPMNMetric> getMetrics() {
	return metrics;
    }

    /**
     * Method calculating score of the diagram using particular metrics. The
     * result is saved into the diagram inner structure.
     *
     * @param d diagram to analyze
     */
    public abstract void calculate(Diagram d);

    public abstract Map<Object, Map<String, Object>> getScores();
}
