/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Radosław O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package bmec.publisher;

/**
 * Class implementing Publisher interface, publishing result as the dokuwiki
 * page.
 *
 * @author izeos (radek dot milo at gmail dot com)
 */
public class WikiPublisher extends MatrixPublisher implements Publisher {

    private static final String HEADER_SIGN = "^";
    private static final String LINE_SIGN = "|";

    @Override
    protected String getFileName() {
	return "page.txt";
    }

    @Override
    protected String getCustomMsg() {
	return "Result table:\n";
    }

    @Override
    protected String getLineBegin() {
	return LINE_SIGN;
    }

    @Override
    protected String getLineEnd() {
	return LINE_SIGN;
    }

    @Override
    protected String getLineSeparator() {
	return LINE_SIGN;
    }

    @Override
    protected String getHeaderBegin() {
	return HEADER_SIGN;
    }

    @Override
    protected String getHeaderEnd() {
	return HEADER_SIGN;
    }

    @Override
    protected String getHeaderSeparator() {
	return HEADER_SIGN;
    }
}
