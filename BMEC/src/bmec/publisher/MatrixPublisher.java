/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Radosław O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package bmec.publisher;

import bmec.metrics.manager.MetricsManager;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Default abstract class providing Publisher implementation for custom
 * Matrix-based structures.
 *
 * @author izeos (radek dot milo at gmail dot com)
 */
public abstract class MatrixPublisher implements Publisher {

    protected abstract String getFileName();

    protected String getCustomMsg() {
        return "";
    }

    protected abstract String getHeaderBegin();

    protected abstract String getHeaderEnd();

    protected abstract String getHeaderSeparator();

    protected abstract String getLineBegin();

    protected abstract String getLineEnd();

    protected abstract String getLineSeparator();

    @Override
    public boolean publish(Map<Object, Map<String, Object>> scores) {

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(getFileName()))) {

            //------------------------------------------------------------
            // intro
            //------------------------------------------------------------
            bw.write(getCustomMsg());
            //------------------------------------------------------------
            // matrix header
            //------------------------------------------------------------
            StringBuilder msgBuilder = new StringBuilder(getHeaderBegin()).append("Diagram");
            for (Iterator<String> it = MetricsManager.getInputMetrics().iterator(); it.hasNext();) {
                String s = it.next();
                msgBuilder.append(getHeaderSeparator()).append(MetricsManager.getMetrics().get(s).getName());
            }
            bw.write(msgBuilder.append(getHeaderEnd()).append("\n").toString());
            //------------------------------------------------------------
            // content
            //------------------------------------------------------------
            for (Object key : scores.keySet()) {
                msgBuilder.delete(0, msgBuilder.length() + 1).append(getLineBegin()).append(key.toString().replaceAll("\n", " "));
                Map<String, ?> map = scores.get(key);
                for (String innerKey : MetricsManager.getInputMetrics()) {
                    msgBuilder.append(getLineSeparator()).append(map.get(innerKey));
                }
                bw.write(msgBuilder.append(getLineEnd()).append("\n").toString());
            }
        } catch (IOException ex) {
            Logger.getLogger(CSVPublisher.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return true;
    }
}
