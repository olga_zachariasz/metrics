/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Radosław O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package bmec;

import bmec.components.Diagram;
import bmec.metrics.manager.MetricsManager;
import bmec.metrics.manager.MetricsManagerImpl;
import bmec.parser.Parser;
import bmec.parser.ParserImpl;
import bmec.publisher.CSVPublisher;
import bmec.publisher.ChartPublisher;
import bmec.publisher.Publisher;
import bmec.publisher.WikiPublisher;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLStreamException;

/**
 * 
 * @author izeos (radek dot milo at gmail dot com)
 */
public class BMECApp {

	private static final long serialVersionUID = 1L;
	private static List<String> metrics;
	private static List<Publisher> publishers;
	/**
	 * Directory for user-defined metrics
	 */
	public static final String METRICS_DIRECTORY = "custommetrics";
	/**
	 * Direcory for user's BPMN files
	 */
	public static final String BPMN_DIRECTORY = "bpmn";

	public BMECApp() {
		publishers = new ArrayList<>();
	}

	/**
	 * Application main method.
	 * 
	 * @param args
	 *            input args.
	 */
	public static void main(String[] args) {
		if (args.length == 0) {
			System.out
					.println("No arguments defined. Use -h to see the application help.");
			return;
		} else {
			System.out.println("Welcome! :D");
		}

		BMECApp application = new BMECApp();
		List<File> files = application.parseArgs(args);
		if (files == null || files.isEmpty() || metrics == null
				|| metrics.isEmpty()) {
			System.out.println("Bye!");
			return;
		}
		List<Diagram> diagrams = application.analyze(files);

		MetricsManager metricsManager = new MetricsManagerImpl(
				METRICS_DIRECTORY, metrics);
		for (Diagram d : diagrams) {
			if (d.getElements().size() > 0)
				metricsManager.calculate(d); // calculates diagram with all
												// metrics on the list
		}

		Map<Object, Map<String, Object>> scores = metricsManager.getScores();

		for (Publisher publisher : publishers) {
			publisher.publish(scores);
		}

		System.out.println("Done. Bye!");
	}

	/**
	 * 
	 * @param files
	 *            list of input files delivered to analysis.
	 * @return List<Diagram> list of diagrams from parsed files.
	 */
	public List<Diagram> analyze(List<File> files) {
		if (files == null || files.isEmpty()) {
			return null;
		}

		List<Diagram> diagrams = new ArrayList<>();
		for (File file : files) {
			try {
				Parser parser = new ParserImpl();
				diagrams.addAll(parser.parse(file));

			} catch (FileNotFoundException ex) {
				Logger.getLogger(BMECApp.class.getName()).log(Level.SEVERE,
						null, ex);
			} catch (XMLStreamException ex) {
				Logger.getLogger(BMECApp.class.getName()).log(Level.SEVERE,
						null, ex);
			} catch (IOException ex) {
				Logger.getLogger(BMECApp.class.getName()).log(Level.SEVERE,
						null, ex);
			}
		}
		return diagrams;
	}

	private List<File> parseArgs(String[] s) {

		// System.out.println("Processing args");
		String msg;
		boolean fileflag = false;
		boolean metricflag = false;
		ArrayList<String> pathes = new ArrayList<>();
		ArrayList<String> names = new ArrayList<>();

		for (int i = 0; i < s.length; i++) {
			String tmp = s[i];
			switch (tmp) {
			case "--help":
			case "-h":
				msg = "BPMN Metric Calculator  Copyright (C) 2012  Radoslaw O. Milo\n"
						+ "This program comes with ABSOLUTELY NO WARRANTY. \n"
						+ "This is free software, and you are welcome to redistribute it \n"
						+ "under certain conditions. Please, see http://www.gnu.org/licenses/\n"
						+ "\n\nApplication Help\n"
						+ "-f \t\t Input Files\n"
						+ "\t\t Add files using BPMN models (*.bpmn), with a proper path if necessary\n\n"
						+ "-m \t\t Names of Metrics\n"
						+ "\t\t Add metrics using classes in metrics directory (e.g. Metric1.class)\n\n"
						+ "-csv \t\t Generate CSV output\n"
						+ "-wiki \t\t Generate Wiki page\n";
				System.out.println(msg);
				return null;
			case "-f":
				fileflag = true;
				continue;
			case "-m":
				metricflag = true;
				continue;
			case "-csv":
				publishers.add(new CSVPublisher());
				continue;
			case "-wiki":
				publishers.add(new WikiPublisher());
				continue;
			case "-chart":
				publishers.add(new ChartPublisher());
				continue;
			case "-df":
				fileflag = true;
				File folder = new File(BPMN_DIRECTORY);

				for (File f : folder.listFiles()) {
					if (f.isFile()) {
						pathes.add(BPMN_DIRECTORY + "/" + f.getName());
					}
				}
				continue;
			case "-dm":
				metricflag = true;
				folder = new File(METRICS_DIRECTORY);
				for (File f : folder.listFiles()) {
					if (f.isFile() && f.getName().endsWith(".class")) {
						names.add(f.getName().replaceFirst(".class", ""));
					}
				}
				continue;
			default:
				if (tmp.endsWith(".bpmn")) {
					pathes.add(tmp);
				} else if (tmp.endsWith(".class")) {
					names.add(tmp.replaceFirst(".class", ""));
				}
			}
		}
		if (!fileflag && pathes.isEmpty()) {
			System.out.println("No -f option used or missing files");
			return null;
		}

		List<File> inputfiles = new ArrayList<>();
		for (String path : pathes) {
			File next = new File(path);
			if (next.exists()) {
				inputfiles.add(next);
			} else {
				System.out.println("Incorrect files path: " + path);
			}
		}

		if (inputfiles.isEmpty()) {
			return null;
		}

		if (!metricflag) {
			System.out.println("No -m used");
		} else {
			metrics = names;
		}

		return inputfiles;
	}
}
