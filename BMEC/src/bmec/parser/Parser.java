/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Radosław O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package bmec.parser;

import bmec.components.Diagram;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import javax.xml.stream.XMLStreamException;

/**
 *
 * @author izeos (radek dot milo at gmail dot com)
 */
public interface Parser {

    /**
     *
     * @param file File to parse
     * @return List<Diagram> list of diagrams in the BPMN model
     * @throws XMLStreamException
     * @throws FileNotFoundException
     */
    public List<Diagram> parse(File file) throws XMLStreamException, FileNotFoundException;
}
