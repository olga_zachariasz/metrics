/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Radosław O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package bmec.parser;

import bmec.components.BPMNModel;
import bmec.components.Diagram;
import bmec.components.ECollection;
import bmec.components.Element;
import bmec.components.FlowElement;
import bmec.components.GatewayElement;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.StartElement;

/**
 * 
 * @author izeos (radek dot milo at gmail dot com)
 */
public class ParserImpl implements Parser {

	private static final String ELEMENTS_DEFINITION = "definition.xml";
	private List<Elem> elementsData;

	public ParserImpl() throws FileNotFoundException, XMLStreamException,
			IOException {
		elementsData = getDefinitions(ELEMENTS_DEFINITION);
	}

	private List<Elem> getDefinitions(String path)
			throws FileNotFoundException, XMLStreamException, IOException {
		List<Elem> elements = new ArrayList<>();

		try (InputStream inputStream = new FileInputStream(new File(path))) {
			XMLInputFactory factory1 = XMLInputFactory.newInstance();
			XMLStreamReader parser1 = factory1
					.createXMLStreamReader(inputStream);

			while (parser1.hasNext()) {
				int event = parser1.next();
				if (event == XMLStreamConstants.START_ELEMENT) {
					// u have to define a printable name for every defined
					// element
					if (!"".equals(parser1.getAttributeValue(0).trim())) {
						elements.add(new Elem(parser1.getLocalName(), parser1
								.getAttributeValue(0), parser1
								.getAttributeValue(1)));
					}
				}
			}
		}
		return elements;
	}

	private String findData(String tag, List<Elem> elements) {
		for (Elem e : elements) {
			if (e.getTag().equals(tag)) {
				return e.getName();
			}
		}
		return null;
	}

	@Override
	public List<Diagram> parse(File file) throws FileNotFoundException,
			XMLStreamException {

		InputStream in = new FileInputStream(file);
		XMLInputFactory factory = XMLInputFactory.newInstance();
		XMLStreamReader parser = factory.createXMLStreamReader(in);
		BPMNModel model = new BPMNModel(file.getName()
				.replaceFirst(".bpmn", ""));
		int counter = 0;
		Map<String, ECollection> elements = model.getDiagrams().get(counter)
				.getElements();
		List<Element> uniqueElements = model.getDiagrams().get(counter)
				.getUniqueElements();
		Map<String, String> attachedElements = model.getDiagrams().get(counter).getAttachedElements();
		String tmp = "";

		// gateways
		boolean isGateway = false;
		int inc = 0;
		int out = 0;

		Element e = null;
		GatewayElement ge = null;
		FlowElement fe = null;
		String name = "";
		String id = "";
		String eventName = "";
		String eventId = "";
		while (parser.hasNext()) {
			String searched = "";
			int event = parser.next();
			if (event == XMLStreamConstants.START_ELEMENT) {
				String startTag = parser.getLocalName();
				name = parser.getAttributeValue("", "name");
				id = parser.getAttributeValue("", "id");
				if ("subProcess".equals(startTag) || "transaction".equals(startTag)) {
					// before going deeper and this subprocess into
					// uniqueElements list
					e = new Element(name, id, startTag);
					uniqueElements.add(e);
					Diagram sub = new Diagram(new StringBuilder(model
							.getDiagrams().get(counter).getName()).append("/")
							.append(parser.getAttributeValue("", "name"))
							.toString());
					model.getDiagrams().add(++counter, sub);
					elements = sub.getElements();
					uniqueElements = sub.getUniqueElements();
					continue;
				} else if (startTag.endsWith("Event")) {
					if(startTag.endsWith("boundaryEvent")) {
						String attachedToId = parser.getAttributeValue("", "attachedToRef");
						attachedElements.put(id, attachedToId);
					}
					tmp = startTag;
					eventName = name;
					eventId = id;
					continue;
				} else if (startTag.endsWith("EventDefinition")) {
					tmp += startTag;
					continue;
				} else {
					searched = startTag;
					if (startTag.endsWith("Gateway")) {
						isGateway = true;
						ge = new GatewayElement(
								name,
								id,
								startTag,
								parser.getAttributeValue("", "gatewayDirection"),
								0, 0);
					} else if (startTag.endsWith("incoming") && isGateway) {
						inc++;
						continue;
					} else if (startTag.endsWith("outgoing") && isGateway) {
						out++;
						continue;
					} else if (startTag.endsWith("sequenceFlow")) {
						String sourceRef = parser.getAttributeValue("",
								"sourceRef");
						String targetRef = parser.getAttributeValue("",
								"targetRef");
						fe = new FlowElement(name, id, startTag, sourceRef,
								targetRef);
					}
				}
			} else if (event == XMLStreamConstants.END_ELEMENT) {
				String actual = parser.getLocalName();
				if (actual.equals("subProcess") || actual.equals("transaction")) {
					--counter;
					elements = model.getDiagrams().get(counter).getElements();
					uniqueElements = model.getDiagrams().get(counter)
							.getUniqueElements();
					searched = actual;
				} else if (actual.endsWith("Event")) {
					searched = tmp;
					tmp = "";
					name = eventName;
					id = eventId;
				} else if (actual.endsWith("Gateway")) {
					ge.setInc(inc);
					ge.setOut(out);
					inc = 0;
					out = 0;
					isGateway = false;
					continue;
				}
			}

			String elementName = findData("" + searched, elementsData);
			if (elementName != null) {
				e = new Element(name, id, searched);
				if (searched.endsWith("Gateway")) {
					uniqueElements.add(ge);
				} else if (searched.endsWith("sequenceFlow")) {
					uniqueElements.add(fe);
				} else if (!searched.endsWith("subProcess")) {
					// subprocesses were already added to uniqueElements so do
					// not do this again
					uniqueElements.add(e);
				}
				name = "";
				id = "";

				ECollection checkedEl = new ECollection(elementName, 1);
				if (!elements.containsKey(searched)) {
					elements.put(searched, checkedEl);
				} else {
					elements.get(searched).incrementCounter();
				}
			}

		}

		return model.getDiagrams();
	}

	private class Elem {

		private String name;
		private String tag;
		private String desc;

		public Elem(String tag) {
			this.tag = tag;
		}

		public Elem(String tag, String name, String desc) {
			this.name = name;
			this.desc = desc;
			this.tag = tag;
		}

		public Elem() {
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getTag() {
			return tag;
		}

		public void setTag(String tag) {
			this.tag = tag;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			final Elem other = (Elem) obj;
			if ((this.tag == null) ? (other.tag != null) : !this.tag
					.equals(other.tag)) {
				return false;
			}
			return true;
		}

		@Override
		public int hashCode() {
			int hash = 7;
			hash = 61 * hash + (this.tag != null ? this.tag.hashCode() : 0);
			return hash;
		}

		@Override
		public String toString() {
			return "Element [name=" + name + ", tag=" + tag + ", desc=" + desc
					+ "]";
		}

	}
}
