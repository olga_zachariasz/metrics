package bmec.components;

public class GatewayElement extends Element {

	private String gatewayType; // join or split
	private int inc; // number of incoming flows
	private int out; // number of outgoing flows

	public GatewayElement(String name, String id, String type,
			String gatewayType, int inc, int out) {
		super(name, id, type);
		this.gatewayType = gatewayType;
		this.inc = inc;
		this.out = out;
	}

	public String getGatewayType() {
		return gatewayType;
	}

	public void setGatewayType(String gatewayType) {
		this.gatewayType = gatewayType;
	}

	public int getInc() {
		return inc;
	}

	public void setInc(int inc) {
		this.inc = inc;
	}

	public int getOut() {
		return out;
	}

	public void setOut(int out) {
		this.out = out;
	}

	@Override
	public String toString() {
		return "GatewayElement [gatewayType=" + gatewayType + ", inc=" + inc
				+ ", out=" + out + ", name=" + getName() + ", id=" + getId()
				+ ", type=" + getType() + "]";
	}

}
