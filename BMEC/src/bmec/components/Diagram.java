/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Radosław O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package bmec.components;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author izeos (radek dot milo at gmail dot com)
 */
public class Diagram {

	private String name;
	private Map<String, ECollection> elements;
	private List<Element> uniqueElements;
	private Map<String, String> attachedElements;

	public Diagram(String name) {
		this.name = name;
		elements = new HashMap<>();
		uniqueElements = new LinkedList<Element>();
		attachedElements = new HashMap<>();
	}

	public List<Element> getUniqueElements() {
		return uniqueElements;
	}

	public void setUniqueElements(List<Element> uniqueElements) {
		this.uniqueElements = uniqueElements;
	}
	

	public Map<String, ECollection> getElements() {
		return elements;
	}

	public void setElements(Map<String, ECollection> elements) {
		this.elements = elements;
	}
	
	public Map<String, String> getAttachedElements() {
		return attachedElements;
	}

	public void setAttachedElements(Map<String, String> attachedElements) {
		this.attachedElements = attachedElements;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return name;
	}
}
