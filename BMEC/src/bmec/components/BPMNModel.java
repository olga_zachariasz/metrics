/**
 * BMEC Project. Application dedicated to create stats based on BPMN diagrams.
 * Copyright (C) 2012 Radosław O. Milo
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package bmec.components;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing BPMN model.
 * 
 * @author izeos (radek dot milo at gmail dot com)
 */
public class BPMNModel {

	List<Diagram> diagrams;

	public BPMNModel(String name) {
		diagrams = new ArrayList<>();
		diagrams.add(new Diagram(name));
	}

	public List<Diagram> getDiagrams() {
		return diagrams;
	}

	public void setDiagrams(List<Diagram> diagrams) {
		this.diagrams = diagrams;
	}
}
