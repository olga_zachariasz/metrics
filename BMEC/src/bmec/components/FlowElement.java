package bmec.components;

public class FlowElement extends Element {

	private String sourceRef;
	private String targetRef;

	public FlowElement(String name, String id, String type, String sourceRef,
			String targetRef) {
		super(name, id, type);
		this.sourceRef = sourceRef;
		this.targetRef = targetRef;
	}

	public String getSourceRef() {
		return sourceRef;
	}

	public void setSourceRef(String sourceRef) {
		this.sourceRef = sourceRef;
	}

	public String getTargetRef() {
		return targetRef;
	}

	public void setTargetRef(String targetRef) {
		this.targetRef = targetRef;
	}

	@Override
	public String toString() {
		return "FlowElement [sourceRef=" + sourceRef + ", targetRef="
				+ targetRef + "]";
	}
	
	
}
